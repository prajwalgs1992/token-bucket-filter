/******************************************************************************/
/* Important CSCI 402 usage information:                                      */
/*                                                                            */
/* This fils is part of CSCI 402 programming assignments at USC.              */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

/*
 * Author:      Prajwal Gandige Sangamesh (gandiges@usc.edu)
 *
 * @(#)$Id: listtest.c,v 1.1 2017/05/27 23:17:27 william Exp $
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include <sys/time.h>

//#include "cs402.h"

#include "my402list.h"


int  My402ListLength(My402List *list)
{
	int length = 0;

	length = list->num_members;
	return length;

}

int  My402ListEmpty(My402List *list)
{
	if (list->num_members != 0)
	{
		return FALSE;
	}
	else { return TRUE; }
}

int My402ListInit(My402List* list)
{
	list->num_members = 0;
	list->anchor.obj = list->anchor.next = list->anchor.prev = NULL;
	return TRUE;
}

My402ListElem *My402ListNext(My402List* list, My402ListElem* elem)
{
	if (elem->next == &(list->anchor) || list->num_members == 0)
	{
		return NULL;
	}
	else {
		return elem->next;
	}
}

int  My402ListInsertAfter(My402List* list, void* obj, My402ListElem* elem)
{

	if (elem == NULL)
	{
		return My402ListAppend(list, obj);
	}

	My402ListElem* tempList = (My402ListElem*)malloc(sizeof(My402ListElem));

	if (tempList != NULL)
	{
		list->num_members++;

		tempList->next = elem->next;
		tempList->prev = elem;
		elem->next->prev = tempList;
		elem->next = tempList;


		tempList->obj = obj;
		return TRUE;
	}
	else {
		return FALSE;
	}
}

int  My402ListAppend(My402List* list, void* obj)
{
	My402ListElem* templist = (My402ListElem *)malloc(sizeof(My402ListElem));

	if (templist == NULL) { return FALSE; }

	if (list->num_members != 0)
	{

		My402ListElem* lastlist;
		lastlist = My402ListLast(list);
		templist->obj = obj;
		templist->next = &(list->anchor);
		templist->prev = lastlist;

		lastlist->next = templist;
		list->anchor.prev = templist;

		list->num_members++;
		return TRUE;
	}
	else {

		templist->obj = obj;
		templist->next = &(list->anchor);
		templist->prev = &(list->anchor);
		list->anchor.next = templist;
		list->anchor.prev = templist;
		list->num_members++;
		return TRUE;

	}
}

int  My402ListInsertBefore(My402List* list, void* obj, My402ListElem* elem)
{
	if (elem == NULL)
	{
		return My402ListPrepend(list, obj);
	}

	My402ListElem* tempList = (My402ListElem*)malloc(sizeof(My402ListElem));

	if (tempList != NULL)
	{
		list->num_members++;

		tempList->next = elem;
		tempList->prev = elem->prev;
		elem->prev->next = tempList;
		elem->prev = tempList;

		tempList->obj = obj;
		return TRUE;
	}
	else {

		return FALSE;
	}
}


My402ListElem *My402ListFirst(My402List* list)
{
	if (list->num_members == 0)
	{
		return NULL;
	}
	else {
		My402ListElem* templist = (My402ListElem *)malloc(sizeof(My402ListElem));
		templist = list->anchor.next;
		return templist;
	}
}

int  My402ListPrepend(My402List* list, void* obj)
{
	My402ListElem* templist = (My402ListElem *)malloc(sizeof(My402ListElem));

	if (templist == NULL) { return FALSE; }

	if (list->num_members != 0)
	{
		My402ListElem* firstlist;
		firstlist = My402ListFirst(list);
		templist->obj = obj;
		templist->next = firstlist;
		templist->prev = &(list->anchor);
		firstlist->prev->next = templist;
		firstlist->next = templist;
		list->num_members++;
		return TRUE;
	}
	else {


		templist->obj = obj;
		templist->next = &(list->anchor);
		templist->prev = &(list->anchor);
		list->anchor.next = templist;
		list->anchor.prev = templist;
		list->num_members++;
		return TRUE;
	}
}

void My402ListUnlink(My402List* list, My402ListElem* elem)
{
	elem->next->prev = elem->prev;
	elem->prev->next = elem->next;
	elem->next = NULL; elem->prev = NULL; elem->obj = NULL;
	list->num_members = list->num_members - 1;
	free(elem);
}

void My402ListUnlinkAll(My402List* list)
{
	My402ListElem* elem = My402ListFirst(list);
	My402ListElem* temp = elem;

	while (temp != NULL)
	{
		My402ListUnlink(list, temp);
		temp = My402ListFirst(list);
	}
	list->anchor.next = list->anchor.prev = NULL;
	list->num_members = 0;
	return;
}


My402ListElem *My402ListLast(My402List* list)
{
	if (list->num_members == 0)
	{
		return NULL;
	}
	else {
		My402ListElem* templist = (My402ListElem *)malloc(sizeof(My402ListElem));
		templist = list->anchor.prev;
		return templist;
	}
}


My402ListElem *My402ListPrev(My402List* list, My402ListElem* elem)
{
	if (list->num_members != 0)
	{
		return elem->prev;
	}
	else {
		return NULL;
	}
}

My402ListElem *My402ListFind(My402List *list, void *obj)
{
	My402ListElem* temp = NULL;
	int length = My402ListLength(list);
	temp = My402ListFirst(list);
	while (length != 0)
	{
		if (temp->obj == obj)
		{
			return(temp);
		}
		length--;
		temp = temp->next;
	}

	return NULL;
}


