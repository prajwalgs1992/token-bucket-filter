#include<stdio.h>
#include<errno.h>
#include<pthread.h>
#include<stdlib.h>
#include"my402list.h"

typedef struct my402pkt
{
	long inter_arr_time;
	long server_time;
	int num_of_tokens,pak_id;
	long service_time,S_enter,S_exit;
	long Q1_enter,Q2_enter,Q1_exit,Q2_exit;
}packet;

My402List Q1,Q2;
//for time calculation
long emulation_begin;
long emulation_finish;
long tbegin,tfinish;
long abegin,afinish;

pthread_mutex_t mlock;pthread_cond_t Q2_cond;
//stats calculation
double avg_iarr_time;
double avg_pktservice_time;
double avg_pktsystem_time;
double SD;
double tkn_drp_prob,pak_drp_prob;
long long avg_pkt_Q1,avg_pkt_Q2,avg_pkt_S1;

//global declarati
long token_arrival_time;
int no_of_packets,token_count,P;
double r,lambda,mu;
int token_bucket,B;
char emode;
int pak_to_arrive;
int server_die,token_die,control_c;
int pak_left_Q1;

FILE *fp;
void parse(int ,char **);
void printstat(double,double,long long,long long,long long,long,double,double,double,double);

