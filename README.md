# README #
Project to simulate/emulate a traffic shaper that transmits packets controlled by a token bucket filter
using multi-threading (POSIX thread library) within a single process and provide detailed statistics about 
the transmission (like packet arrival rate, packet drop probability, inter-arrival time etc).