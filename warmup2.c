#include<stdio.h>
#include<unistd.h>
#include<sys/time.h>
#include<math.h>
#include<stdlib.h>
#include<pthread.h>
#include<string.h>
#include<signal.h>
#include"warmup2.h"

void print_to_screen(long time1)
{
	double t;
	t = (double)(time1-emulation_begin)/1000;
	fprintf(stdout,"%012.3f:",t);
}

long gettime()
{
   struct timeval tim;
   gettimeofday(&tim,NULL);
   return((tim.tv_sec*1000000)+tim.tv_usec);
}

void interrupt()
{
	token_die = 1;
	print_to_screen(gettime());	 
	fprintf(stdout,"SIGINT caught, no new packets or tokens will be allowed\n");
	control_c = 1;
}

void readfrom_file(long *arrival_t,long *service_t,int *token)
{
	long arrival_time;long service_time;
	char buffer[1026];
	if(fgets(buffer,sizeof(buffer),fp)!=NULL)
	{
	  if(buffer[0] == ' ' || buffer[0] == '\t')
		{
		sleep(1);
		fprintf(stderr,"\n error in file format\n leading whitespace\n");
		exit(1);}
	if(buffer[strlen(buffer)-2] == ' ' || buffer[strlen(buffer)-2] == '\t')
		{
		sleep(1);
		fprintf(stderr,"\n error in file format\n trailing whitespace\n");
		exit(1);}
	
	sscanf(buffer,"%ld%d%ld",&arrival_time,token,&service_time);
	*arrival_t = arrival_time*1000;
	*service_t = service_time*1000;
	}
}
		
void modecheck(long *arrival_t,long *service_t,int *token)
{
	if(emode == 't')
	{
	readfrom_file(arrival_t,service_t,token);
	}else{*arrival_t = ((1/lambda)/1000000);
		*service_t = ((1/mu)/1000000);*token = P;}
}

void *arrival_func(void *args)
{
	int i,token_required;
	int pak_incoming = 0;
	int pdrop = 0;
	long temp_time,prev_pak_time = 0;
	long avg_ia = 0;
	packet *pak;
	My402ListElem *elem;
	long sleep_time,p_sleep_time,p_service_time;

	for(i=0;i<no_of_packets;i++)
	{
	if(control_c == 1)
	{
	i = i - 1;
	server_die = token_die = 1;break;
	}
	modecheck(&p_sleep_time,&p_service_time,&token_required);

	afinish = gettime();
	if(p_sleep_time>(afinish-abegin))
	{
	sleep_time = p_sleep_time - (afinish-abegin);
	usleep(sleep_time);}
	//printf("--------after sleep-------------\n");
	pak_incoming++;
	pak = (packet *)malloc(sizeof(struct my402pkt));
	pak->num_of_tokens = token_required;
	pak->pak_id = i+1;
	pak->service_time=p_service_time;
	pak->S_enter=abegin=gettime();
	temp_time = gettime();
	if(prev_pak_time == 0)
	{
	prev_pak_time = pak->inter_arr_time = (temp_time-emulation_begin);
	prev_pak_time = temp_time;
	}else{
	pak->inter_arr_time = (temp_time-prev_pak_time);prev_pak_time=temp_time;}
	avg_ia = avg_ia+pak->inter_arr_time;
	
	if(pak->num_of_tokens>B)
	{
	pak_to_arrive = pak_to_arrive-1;
	pdrop = pdrop + 1;
	print_to_screen(gettime());
fprintf(stdout,"p%d arrives, invalid token requirement,p%d Dropped!!\n",pak->pak_id,pak->pak_id);continue;
	}else{
	print_to_screen(gettime());
fprintf(stdout,"p%d arrives,needs %d tokens, inter-arrival time = %.3fms\n",pak->pak_id,pak->num_of_tokens,(double)(pak->inter_arr_time)/1000000);
}

if(control_c == 1)
{
server_die=token_die=1;break;
}
pthread_mutex_lock(&mlock);
if(My402ListEmpty(&Q1))
{
  My402ListAppend(&Q1,pak);
	//printf("--------appended-------------\n");
  pak->Q1_enter=gettime();
  print_to_screen(gettime());
  fprintf(stdout,"p%d enters Q1\n",pak->pak_id);
	if(!My402ListEmpty(&Q2))
	{
	pthread_cond_signal(&Q2_cond);
	}
	if(token_bucket>=pak->num_of_tokens)
	{
	elem = My402ListFirst(&Q1);
	//printf("--------unlink from Q1------------\n");
	pak=(packet *)elem->obj;
	My402ListUnlink(&Q1,elem);
	pak->Q1_exit = gettime();
	token_bucket = token_bucket-pak->num_of_tokens;
	print_to_screen(gettime());
fprintf(stdout,"p%d leaves Q1,time in Q1 = %.3f ms,token bucket now has %d tokens\n",pak->pak_id,(double)(pak->Q1_exit-pak->Q1_enter)/1000,token_bucket);
	avg_pkt_Q1 = avg_pkt_Q1 + (pak->Q1_exit-pak->Q1_enter);
	//enqueue to Q2
	pak->Q1_enter = pak->Q1_exit = 0;	
	pak_left_Q1 = pak_left_Q1 + 1;
My402ListAppend(&Q2,pak);pak->Q2_enter=gettime();
print_to_screen(gettime());
fprintf(stdout,"p%d enters Q2\n",pak->pak_id);
pthread_cond_signal(&Q2_cond);
}		
}
else{
	My402ListAppend(&Q1,pak);
	pak->Q1_enter=gettime();
	print_to_screen(gettime());
	fprintf(stdout,"p%d enters Q1\n",pak->pak_id);
}
	pthread_mutex_unlock(&mlock);
}
	pthread_mutex_lock(&mlock);pthread_cond_signal(&Q2_cond);
	pthread_mutex_unlock(&mlock);

	if(pak_incoming==0){avg_iarr_time=pak_drp_prob=0;}
	else{ pak_drp_prob = (double)pdrop/pak_incoming;
	avg_iarr_time = (double)((double)avg_ia/(double)((pak_incoming)*1000000));}
	pthread_exit(NULL);
}

void *token_func(void *args)
{
	packet *pak;
	My402ListElem *elem;
	int token_drop=0;
	int Q2_empty;
	long sleep_time;
	double timeinQ1;
	while(1)
        {
	  if(token_die == 1)
           {
		tkn_drp_prob = (double)(token_drop/token_count);
		server_die = 1;
		pthread_mutex_lock(&mlock);pthread_cond_signal(&Q2_cond);
		pthread_mutex_unlock(&mlock);
		pthread_exit(NULL);}
	tfinish=gettime();
	sleep_time=token_arrival_time-(tfinish-tbegin);
	usleep(sleep_time);	
	tbegin=gettime();
	pthread_mutex_lock(&mlock);
	
	if(token_bucket<B)
	{
	token_bucket = token_bucket + 1;
	token_count = token_count + 1;
	print_to_screen(gettime());
	fprintf(stdout,"token t%d arrives,token bucket now has %d tokens\n",token_count,token_bucket);}else{
	print_to_screen(gettime());
	token_count = token_count+1;token_drop = token_drop+1;
	fprintf(stdout,"token t%d arrives, token bucket is full,token dropped!!\n",token_count);
}

if(!My402ListEmpty(&Q1))
{
	elem = My402ListFirst(&Q1);
	pak = (packet *)elem->obj;
	if(token_bucket>=pak->num_of_tokens)
	{
	token_bucket = token_bucket-pak->num_of_tokens;
	My402ListUnlink(&Q1,elem);
	print_to_screen(gettime());
	pak->Q1_exit=gettime();
	timeinQ1=(double)((pak->Q1_exit-pak->Q1_enter)/1000);
	fprintf(stdout,"p%d leaves Q1,time in Q1 = %.3fms,token bucket now has %d tokens\n",pak->pak_id,timeinQ1,token_bucket);
	avg_pkt_Q1 = avg_pkt_Q1 - (pak->Q1_exit-pak->Q1_enter);
	pak_left_Q1 = pak_left_Q1 + 1;
	//q2 empty check
	Q2_empty = My402ListEmpty(&Q2);
	My402ListAppend(&Q2,pak);
	pak->Q2_enter = gettime();
	print_to_screen(gettime());
	fprintf(stdout,"p%d enters Q2\n",pak->pak_id);
	if(Q2_empty == 1)
	{
	 pthread_cond_signal(&Q2_cond);
	}
      }
  }
	pthread_mutex_unlock(&mlock);
	if(pak_left_Q1 == pak_to_arrive)
	{
	  tkn_drp_prob = (double)token_drop/token_count;
	  pthread_exit(NULL);
	}
}
}

void *server_func(void *args)
{
   packet *pak;
   My402ListElem *elem;
   double timeinQ2;
   long sstart,ssend;
   double servertime;
   long avg_st = 0,avg_pksyst=0; 
    int packets_pro = 0;
	double variance1,variance2;
	long time_in_system;
    double sys_avg_1=0,sys_avg_2=0;			
	int itr=0;

	for(itr=0;itr<pak_to_arrive;itr++)
	{
	pthread_mutex_lock(&mlock);
	while(My402ListEmpty(&Q2) && server_die == 0)
	{
	if(itr==pak_to_arrive){break;}
	pthread_cond_wait(&Q2_cond,&mlock);
	}
	if(itr==pak_to_arrive){server_die=1;}
	if(server_die!=0)
	{
	  My402ListUnlinkAll(&Q1);My402ListUnlinkAll(&Q2);
	  pthread_mutex_unlock(&mlock);break;
	}
	elem=My402ListFirst(&Q2);
	pak=(packet *)elem->obj;
	My402ListUnlink(&Q2,elem);
	pak->Q2_exit=gettime();
	timeinQ2=(double)((pak->Q2_exit-pak->Q2_enter)/1000);
	print_to_screen(gettime());
	fprintf(stdout,"p%d leaves Q2,time in Q2 =%.3fms\n",pak->pak_id,timeinQ2);

	print_to_screen(gettime());
	fprintf(stdout,"p%d begins service at S,requesting %ldms of service\n",pak->pak_id,pak->service_time);

	pthread_mutex_unlock(&mlock);
	avg_pkt_Q2 = avg_pkt_Q2 + (pak->Q2_exit-pak->Q2_enter);
	sstart=gettime();
	usleep(pak->service_time);
	pak->S_exit=ssend=gettime();
	pak->server_time=(ssend-sstart);
	avg_st = avg_st+pak->server_time;
	avg_pksyst=avg_pksyst+(pak->S_exit-pak->S_enter);
	packets_pro = packets_pro+1;
	servertime = (double)(pak->server_time)/1000;
	time_in_system=(pak->S_exit-pak->S_enter);
	print_to_screen(gettime());
	fprintf(stdout,"p%d departs from S, service time=%.3fms,",pak->pak_id,servertime);
	fprintf(stdout,"time in System = %.3f ms\n",(double)(time_in_system)/1000);
	
	//aveerage calculation
	avg_pkt_S1 = avg_pkt_S1+pak->server_time;
	sys_avg_1 = sys_avg_1 + (double)(time_in_system)/1000000;
	sys_avg_2 = sys_avg_2 + ((double)(time_in_system)/1000000) * ((double)(time_in_system)/1000000);
	}
 		
	if(packets_pro==0)
	{
	avg_pktservice_time = avg_pktsystem_time = SD =0;
	variance1=variance2=0;
	}else{
	avg_pktservice_time = (double)((double)avg_st/(double)(packets_pro*1000000));	
	avg_pktsystem_time =(double)((double)avg_pksyst/(double)(packets_pro*1000000));		
	
	variance1=sys_avg_1/packets_pro;
	variance2=sys_avg_2/packets_pro;
	SD = sqrt(variance2-(variance1*variance1));
	}
	token_die=1;pthread_exit(NULL);
}

int main(int argc,char **argv)
{
	pthread_t arr_thread,tkn_thread,serv_thread1;
	long total_emu_time;
	//signal handler
	sigset_t set;
	struct sigaction act;
	
	//default command line args
	token_bucket = 0;P=3;no_of_packets=20;
	r=1.5;lambda=1.0;mu=0.35;B=10;

	avg_pkt_Q1 = avg_pkt_Q2 = avg_pkt_S1 = 0;
	server_die = token_die = 0;control_c = 0;

	sigemptyset(&set);sigaddset(&set,SIGINT);
	pthread_sigmask(SIG_BLOCK,&set,NULL);
	parse(argc,argv);
	pak_to_arrive=no_of_packets;
	token_arrival_time=(long)((1/r)*1000000);

	//thread creation
	pthread_create(&arr_thread,NULL,arrival_func,NULL);
	pthread_create(&tkn_thread,NULL,token_func,NULL);
	pthread_create(&serv_thread1,NULL,server_func,NULL);
	
	act.sa_handler=interrupt;
	sigaction(SIGINT,&act,NULL);
	pthread_sigmask(SIG_UNBLOCK,&set,NULL);
	emulation_begin=tbegin=abegin=gettime();
	print_to_screen(gettime());
	fprintf(stdout,"emulation begins\n");
	
	//joining with the main thread
	pthread_join(arr_thread,NULL);
	pthread_join(tkn_thread,NULL);
	pthread_join(serv_thread1,NULL);

	emulation_finish = gettime();
	print_to_screen(gettime());
	fprintf(stdout,"emulation ends\n");
	total_emu_time = emulation_finish - emulation_begin;
	//print statistics
	printstat(avg_iarr_time,avg_pktservice_time,avg_pkt_Q1,avg_pkt_Q2,avg_pkt_S1,total_emu_time,avg_pktsystem_time,SD,tkn_drp_prob,pak_drp_prob);

	pthread_mutex_destroy(&mlock);
	pthread_cond_destroy(&Q2_cond);
	pthread_exit(NULL);
}

